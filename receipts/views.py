from django.shortcuts import redirect, render

from receipts.models import Account, ExpenseCategory, Receipt
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView

from django.contrib.auth.mixins import LoginRequiredMixin


class ReceiptListView(ListView):
    model = Receipt
    template_name = "receipts/list.html"


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/new.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    success_url = reverse_lazy("home")

    #def get_success_url(self):
        #return reverse_lazy("home", args=[self.object.id])

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("home")

        #form.instance.user = self.request.user
        #return super().form_valid(form)

        #i think i will have to change the .author here?


class ReceiptAccountListView(ListView):
    model = Account
    template_name = "accounts/list.html"


class ReceiptAccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "accounts/new.html"
    fields = ["name", "number"]
    success_url = reverse_lazy("receipt_account_list")

    def form_valid(self, form):
        item = form.save(commit=False)
        # this returns an object that hasn't yet been saved to the database
        item.owner = self.request.user
        item.save()
        return redirect("receipt_account_list")


class ReceiptCategoryListView(ListView):
    model = ExpenseCategory
    template_name = "categories/list.html"


class ReceiptCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "categories/new.html"
    fields = ["name"]

    def form_valid(self, form):
        item = form.save(commit=False)
        # this returns an object that hasn't yet been saved to the database
        item.purchaser = self.request.user
        item.save()
        return redirect("receipt_account_list")
