from tkinter import CASCADE
from django.db import models

from accounts.models import USER_MODEL

# Create your models here.

class Account(models.Model):
    name = models.CharField(max_length = 100, null=True)
    number = models.CharField(max_length = 20, null=True)
    owner = models.ForeignKey(USER_MODEL, related_name="accounts", on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name

class ExpenseCategory(models.Model):
    name = models.CharField(max_length = 50, null=True)
    owner = models.ForeignKey(USER_MODEL, related_name="categories", on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name

class Receipt(models.Model):
    vendor = models.CharField(max_length = 200, null=True)
    total = models.DecimalField(decimal_places=3, max_digits=10, null=True)
    tax = models.DecimalField(decimal_places=3, max_digits=10, null=True)
    date = models.DateTimeField(null=True)
    category = models.ForeignKey("ExpenseCategory", related_name="receipts", on_delete=models.CASCADE, null=True, blank=True)
    account = models.ForeignKey(Account, related_name="receipts", on_delete=models.CASCADE, blank=True, null=True)
    purchaser = models.ForeignKey(USER_MODEL, related_name="receipts", on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.vendor

#null = True is database side of data.. blank = true is form side of data
