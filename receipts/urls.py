from django.urls import path

from receipts.views import ReceiptAccountCreateView, ReceiptAccountListView, ReceiptCategoryCreateView, ReceiptCategoryListView, ReceiptCreateView, ReceiptListView

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="receipt_new"),
    path("accounts/", ReceiptAccountListView.as_view(), name="receipt_account_list"),
    path("accounts/create/", ReceiptAccountCreateView.as_view(), name="receipt_account_new"),
    path("categories/", ReceiptCategoryListView.as_view(), name="receipt_category_list"),
    path("categories/create/", ReceiptCategoryCreateView.as_view(), name="receipt_category_new"),
]
