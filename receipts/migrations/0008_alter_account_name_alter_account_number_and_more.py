# Generated by Django 4.0.3 on 2022-05-05 22:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('receipts', '0007_alter_receipt_category'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='name',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='account',
            name='number',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='receipt',
            name='date',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='receipt',
            name='tax',
            field=models.DecimalField(decimal_places=3, max_digits=10, null=True),
        ),
        migrations.AlterField(
            model_name='receipt',
            name='total',
            field=models.DecimalField(decimal_places=3, max_digits=10, null=True),
        ),
        migrations.AlterField(
            model_name='receipt',
            name='vendor',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
